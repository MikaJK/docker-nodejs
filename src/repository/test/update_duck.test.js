import { create_duck, update_duck }  from '../duckRepository';
var duey = new Object();
duey.name = "Duey Duck";
var huey = new Object();
huey.name = "Huey Duck";
var luey = new Object();
luey.id = "bbb-ccc-ddd";
luey.name = "Luey Duck";

describe('UPDATE DUCK', () => {
    it('should update duck', () => {
        var created_duck = create_duck(duey);
        expect(created_duck).not.toBeNull();
        huey.id = created_duck.id;
        var updated_duck = update_duck(created_duck.id, huey);
        expect(updated_duck).not.toBeNull();
        expect(updated_duck.id).toBe(created_duck.id);
        expect(updated_duck.name).toBe(huey.name);
    });

    it('should update duck without id', () => {
        var created_duck = create_duck(duey);
        expect(created_duck).not.toBeNull();
        huey.id = undefined;
        var updated_duck = update_duck(created_duck.id, huey);
        expect(updated_duck).not.toBeNull();
        expect(updated_duck.id).toBe(created_duck.id);
        expect(updated_duck.name).toBe(huey.name);
    });

    it('should not update duck', () => {
        expect(update_duck(luey.id, luey)).toBe(undefined);
    });
});