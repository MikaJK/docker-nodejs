import { create_duck }  from '../duckRepository';
var duey = new Object();
duey.name = "Duey Duck";

describe('CREATE DUCK', () => {
    it('should create duck', () => {
        var result = create_duck(duey);
        expect(result).not.toBeNull();
        expect(result.id).not.toBeNull();
        expect(result.name).toBe(duey.name);
    });
});