import { create_duck, delete_duck }  from '../duckRepository';
var duey = new Object();
duey.name = "Duey Duck";

describe('DELETE DUCK', () => {
    it('should delete duck', () => {
        var created_duck = create_duck(duey);
        expect(created_duck).not.toBeNull();
        expect(delete_duck(created_duck.id)).toBe(true);
    });

    it('should not delete duck', () => {
        expect(delete_duck("ccc-ddd-eee")).toBe(false);
    });
});