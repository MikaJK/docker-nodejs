import { get_ducks, create_duck }  from '../duckRepository';
var duey = new Object();
duey.name = "Duey Duck";
var huey = new Object();
huey.name = "Huey Duck";
var luey = new Object();
luey.name = "Luey Duck";

describe('GET DUCKS', () => {
    it('should get zero ducks', () => {
        expect(get_ducks.length).toBe(0);
    });

    it('should get list of ducks', () => {
        expect(create_duck(duey)).not.toBeNull();
        expect(create_duck(huey)).not.toBeNull();
        expect(create_duck(luey)).not.toBeNull();
        expect(get_ducks().length).toBe(3);
    });
});