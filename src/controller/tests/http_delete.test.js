import '@babel/polyfill';
import app from '../../app';
import supertest from 'supertest';
const deleteTestRequest = supertest(app);
var duck = {
    name: "Duey Duck"
};
var id;

describe('DELETE', () => {
    it('should create a duck', async done => {
        await deleteTestRequest.post('/ducks').send(duck)
            .then((response) => {
                expect(response.statusCode).toBe(201);
                expect(response.body.id).not.toBeNull();
                expect(response.body.name).toBe(duck.name);
                id = response.body.id;
                done();
            });
    });

    it('should delete duck', async done => {
        await deleteTestRequest.delete(`/ducks/${id}`)
            .then((response) => {
                expect(response.statusCode).toBe(204);
                done();
            });
    });

    it('should get zero duck', async done => {
        await deleteTestRequest.get(`/ducks/${id}`)
            .then((response) => {
                expect(response.statusCode).toBe(404);
                done();
            });
    });

    it('should get error', async done => {
        id = "bbb-ccc-ddd";
        await deleteTestRequest.delete(`/ducks/${id}`)
            .then((response) => {
                expect(response.statusCode).toBe(404);
                done();
            });
    });
});