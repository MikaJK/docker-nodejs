import '@babel/polyfill';
import app  from '../../app';
import supertest from 'supertest';
const postTestRequest = supertest(app);
var duck = {
    name: "Duey Duck"
};
var id;

describe('POST', () => {
    it('should create a duck', async done => {
        await postTestRequest.post('/ducks').send(duck)
            .then((response) => {
                expect(response.statusCode).toBe(201);
                expect(response.body.id).not.toBeNull();
                expect(response.body.name).toBe(duck.name);
                id = response.body.id;
                done();
            });
    });

    it('should get one duck', async done => {
        await postTestRequest.get(`/ducks/${id}`)
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.id).not.toBeNull();
                expect(response.body.name).toBe(duck.name);
                done();
            });
    });

    it('should get error', async done => {
        
        await postTestRequest.post(`/bucks`).send(duck)
            .then((response) => {
                expect(response.statusCode).toBe(404);
                done();
            });
    });
});