import '@babel/polyfill';
import app  from '../../app';
import supertest from 'supertest';
const getAllTestRequest = supertest(app);
var ducks = [
    {
        name: "Duey Duck"
    },
    {
        name: "Huey Duck"
    },
    {
        name: "Luey Duck"
    }
]

describe('GET ALL', () => {
    it('should get zero ducks', async done => {
        await getAllTestRequest.get('/ducks')
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.length).toBe(0);
                done();
            });
    });

    it('should get list of ducks', async done => {
        await getAllTestRequest.post('/ducks').send(ducks[0])
            .then((response) => {
                expect(response.statusCode).toBe(201);
                done();
            });

        await getAllTestRequest.post(`/ducks`).send(ducks[1])
            .then((response) => {
                expect(response.statusCode).toBe(201);
                done();
            });

        await getAllTestRequest.post(`/ducks`).send(ducks[3])
            .then((response) => {
                expect(response.statusCode).toBe(201);
                done();
            });

        await getAllTestRequest.get(`/ducks`)
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.length).toBe(3);
                done();
            });
    });

    it('should get error', async done => {
        await getAllTestRequest.get('/bucks')
            .then((response) => {
                expect(response.statusCode).toBe(404);
            });

        done();
    });
});