# docker-nodejs

## Dockerfile

FROM node:10

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
\# RUN npm ci --only=production

COPY . .

RUM npm run build

EXPOSE 8080
CMD [ "npm", "start" ]

## .dockerignore

node_modules
npm-debug.log
.ebextensions
.elasticbeanstalk
.idea
coverage
bitbucket-pipelines.yml

## Build Image

docker build -t nodejs-rest-api .

## List Images

docker images

## Run Container

docker run -p 22569:8080 -d nodejs-rest-api

## Stop Container

docker stop <image_id>

## Remove Container

docker rm <image_id>